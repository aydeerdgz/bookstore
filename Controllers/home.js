var model = require('../model/Book');
var mongoose = require('mongoose')
Book = mongoose.model('bookcollections')

    
    exports.listAll= function(req, res) {
        Book.find({}, function(err, book) {
                if(err)
                res.send(err)

                res.end(JSON.stringify(book));
            });
    };

    exports.listByPage= function(req, res) {
        var patharray = req.requrl.path.split('/');

        if(patharray[5] === "html")
        res.writeHead(200, {'Content-Type': 'text/html'});

        else res.writeHead(200, {'Content-Type': 'text/plain'});
        
        
        Book.find({No: patharray[2]}, function (err, Obj) {
            if (err) {
                console.log(err)
            }

            if(!Obj.length) {
                res.write("404 Not Found\n");
                res.end();
            }
            
            var foundit = 0;
            var page = Obj[0].Pages[0];
            var final = "Page"+patharray[4]

            for(var key in page) {
                if(key === final) {
                    foundit=1;
                    res.end(JSON.stringify(page[key]))
                }
            }
            
            if(foundit===0) {
            res.write("404 Not Found\n");
            res.end();
            }  
        });
    }

    exports.Error= function(req, res) {
        res.writeHead(404, {"Content-Type": "text/plain"});
        res.write("404 Not Found\n");
        res.end();
    };

