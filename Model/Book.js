'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BookSchema = new Schema({
    No: 'String',
    Name: 'String',
    Date: 'String',
    NoPages: 'String',
    Pages: {type: Array , "default": []}
});

module.exports = mongoose.model('bookcollections', BookSchema)