var book = require('../Model/Book.js');
var mongoose = require('mongoose');

mongoose.Promise = global.Promise;
var connectMongoOnline='mongodb://books:hola12345678@ds257470.mlab.com:57470/books';
mongoose.connect(connectMongoOnline,{ useNewUrlParser: true });

var books = [
    new book({
        "No": "5",
        "Name": "The Shining",
        "Date": "22-11-1998",
        "NoPages": "2",
        "Pages": [
            {
                "Page1": "Ipsum Lempson Cat",
                "Page2": "Ipsum Lempson"
            }
        ]
    }),
    new book({
        "No": "6",
        "Name": "IT",
        "Date": "22-11-1998",
        "NoPages": "2",
        "Pages": [
            {
                "Page1": "Ipsum Lempson Cat",
                "Page2": "Ipsum Lempson"
            }
        ]
    }),
];

var done = 0;
for( var i=0; i< books.length; i++){
    books[i].save(function(err, result){
        done++;
        if (done === books.length){
            exit();
        }
    });
}

function exit() {
    mongoose.disconnect();
}


