var url = require('url');
var fs = require('fs');

exports.get = function(req, res) {
    req.requrl = url.parse(req.url, true);
    var path = req.requrl.pathname;
    var patharray = path.split('/')

    if(path === '/') {
        fs.readFile('./Views/home.html', function(err, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            res.end();
        });
    }

    else if(path === "/listall") {
        require('./controllers/home').listAll(req,res);
    }

    else if (path === "/book/"+patharray[2]+"/page/"+patharray[4]+"/"+patharray[5]) {
        require('./controllers/home').listByPage(req,res);       
    }

    else {
        require('./controllers/home').Error(req,res);  
    }

    }